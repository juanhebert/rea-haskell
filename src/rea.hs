import qualified Data.HashMap.Strict as M
import Data.Bifunctor (first, second)
import Control.Monad ((>=>))
import Data.Tuple (swap, uncurry)
import Data.List (groupBy, sortOn, union)

class Monoid a => LeftMul a where
    (<.>) :: Double -> a -> a

neg :: LeftMul a => a -> a
neg = ((-1) <.>)

sub :: (LeftMul a) => a -> a -> a
sub x y = x <> neg y

instance Semigroup Double where
    (<>) = (+)

instance Monoid Double where
    mempty = 0
    mappend = (<>)

instance LeftMul Double where
    (<.>) = (*)

type Actor = String
type ResourceType = String

-- Resources

newtype Resource = Resource { getResource :: M.HashMap ResourceType Double }
    deriving (Show, Eq)

instance Semigroup Resource where
    (<>) (Resource r1) (Resource r2) =
        Resource $ M.filter (/= 0) $ M.unionWith (+) r1 r2

instance Monoid Resource where
    mempty = Resource M.empty
    mappend = (<>)

instance LeftMul Resource where
    (<.>) k = Resource . M.map (k <.>) . getResource

isZero :: (Monoid a, Eq a) => a -> Bool
isZero = (== mempty)

makeResource :: [(ResourceType, Double)] -> Resource
makeResource = Resource
    . M.filter (not . isZero)
    . M.fromList


-- Ownership states

newtype OwnershipState =
    OwnershipState { getOwnershipState :: M.HashMap Actor Resource }
    deriving (Show, Eq)

instance Semigroup OwnershipState where
    (<>)(OwnershipState s1) (OwnershipState s2) =
        OwnershipState $ M.filter (not . isZero) $ M.unionWith (<>) s1 s2

instance Monoid OwnershipState where
    mempty = OwnershipState M.empty
    mappend = (<>)

instance LeftMul OwnershipState where
    (<.>) k = OwnershipState . M.map (k <.>) . getOwnershipState

nullOwnershipState :: OwnershipState -> Bool
nullOwnershipState = null . getOwnershipState

makeOwnershipState :: [(Actor, Resource)] -> OwnershipState
makeOwnershipState = OwnershipState
    . M.filter (not . isZero)
    . M.fromList

makeOwnershipState' :: [(Actor, [(ResourceType, Double)])] -> OwnershipState
makeOwnershipState' = makeOwnershipState . map (second makeResource)

support :: OwnershipState -> [Actor]
support = M.keys . getOwnershipState

supportWith :: (Monoid a, Eq a) => (Resource -> a) -> OwnershipState -> [Actor]
supportWith w = M.keys . M.filter (not . isZero . w) . getOwnershipState

sumOwnershipState :: OwnershipState -> Resource
sumOwnershipState = M.foldl' (<>) mempty . getOwnershipState


-- Transfers

type Transfer = OwnershipState -- zero-sum property enforced via isTransfer

isTransfer :: OwnershipState -> Bool
isTransfer = isZero . sumOwnershipState

isTwoParty :: OwnershipState -> Bool
isTwoParty = (\l -> length l == 2) . M.keys . getOwnershipState

commitTransfer :: (OwnershipState -> Bool) -> Transfer -> OwnershipState
    -> Maybe OwnershipState
commitTransfer p t s = if p s' then Just s' else Nothing
    where s' = s <> t

-- Note: Kleisli composition as defined in section 3.2 is equivalent to the
-- (>=>) function in Control.Monad
kleisli :: (OwnershipState -> Maybe OwnershipState)
    -> (OwnershipState -> Maybe OwnershipState)
    -> (OwnershipState -> Maybe OwnershipState)
kleisli = (>=>)


-- Events

type Event = OwnershipState -- zero-sum property w.r.t w enforced via isEvent

-- Pre-condition: w must be a homomorphism
isEvent :: (Monoid a, Eq a) => (Resource -> a)  -> OwnershipState -> Bool
isEvent w = isZero . w . sumOwnershipState

isTransformation :: (Monoid a, Eq a) => (Resource -> a) -> OwnershipState
    -> Bool
isTransformation w = M.foldl' f True . getOwnershipState
    where f acc curr = if isZero $ w curr then acc else False

commitEvent :: (OwnershipState -> Bool) -> (Event -> Bool) -> Event
    -> OwnershipState
    -> Maybe OwnershipState
commitEvent p q e s = if p s' && q e then Just s' else Nothing
    where s' = s <> e

-- Note that we have commitTransfer p = commitEvent p (\_ -> True)

decomposeEventLoop :: OwnershipState -> [Transfer]
    -> ([Transfer], Event)
decomposeEventLoop e ts
    | M.size (getOwnershipState e) > 1 =
        let
            [(a, r1), (b, _)] = take 2 $ M.toList $ getOwnershipState e
            t = makeOwnershipState [(a, r1), (b, neg r1)]
            ts' = t:ts
            e' = sub e t
        in
            decomposeEventLoop e' ts'
    | otherwise = (ts, e)

-- Pre-condition: t must be a two party transfer
twoPartyToSingleResource :: Transfer -> [Transfer]
twoPartyToSingleResource t =
    let
        [(a, r1), (b, _)] = M.toList $ getOwnershipState t
        makeTransfer (k, v) = makeOwnershipState' [
                (a, [(k, v)]),
                (b, [(k, -v)])
            ]
    in map makeTransfer $ M.toList $ getResource r1

decomposeEvent :: Event -> ([Transfer], Event)
decomposeEvent e =
    first (>>= twoPartyToSingleResource) $ decomposeEventLoop e []

decomposeTransfer :: Transfer -> [Transfer]
decomposeTransfer = fst . decomposeEvent

fuse :: ([Transfer], Event) -> [Event]
fuse (ts, e) | e == mempty = ts
             | otherwise = e:ts

-- Pre-condition: t must be either a single-resource two-party transfer
-- or a one-party transformation.
spender :: Transfer -> Actor
spender t =
    let
        ((a, r1):ts) = M.toList $ getOwnershipState t
        [(k, v)] = M.toList $ getResource r1
    in
        case ts of
            ((b, _):[]) -> if v < 0 then a else b
            [] -> a

-- Pre-condition: t must be either a single-resource two-party transfer
-- or a one-party transformation.
recipient :: Transfer -> Actor
recipient t =
    let
        ((a, r1):ts) = M.toList $ getOwnershipState t
        [(k, v)] = M.toList $ getResource r1
    in
        case ts of
            ((b, _):[]) -> if v > 0 then a else b
            _ -> a

sameSpender :: Transfer -> Transfer -> Bool
sameSpender t1 t2 = spender t1 == spender t2

type TransactionId = Integer

taggedGroupBySpender :: TransactionId -> [Transfer]
    -> [(TransactionId, Actor, Transfer)]
taggedGroupBySpender i =
    (map (\ts -> (i, spender $ head ts, mconcat ts)))
    . groupBy sameSpender

prependGroupSpender :: [(TransactionId, Actor, Transfer)]
    -> (Actor, [(TransactionId, Transfer)])
prependGroupSpender ts@((_, a, _):_) = (a, map (\ (a, _, b) -> (a, b)) ts)

mergeByTransaction :: [(TransactionId, Actor, Transfer)]
    -> [(TransactionId, Actor, Transfer)]
mergeByTransaction =
    map combine
    . groupBy (\a b -> getId a == getId b)
    . sortOn getId
    where
        getId (a, _, _) = a
        op (i1, a1, t1) (_, _, t2) = (i1, a1, t1 <> t2)
        combine = foldl1 op

positiveBalance :: Resource -> Bool
positiveBalance = foldl f True . M.toList . getResource
    where f acc (_, v) = v >= 0 && acc

allBalancesPositive :: OwnershipState -> Bool
allBalancesPositive =
    (== mempty)
    . M.filter (not . positiveBalance)
    . getOwnershipState

getBalance :: Actor -> OwnershipState -> Resource
getBalance a = M.lookupDefault mempty a . getOwnershipState

-- Given the initial set of pending transactions, tagged with their ids,
-- this function generates the initial state of the algorithm.
makeNettingQueues :: [(TransactionId, Event)]
    -> [(Actor, [(TransactionId, Transfer)])]
makeNettingQueues =
    map (second (sortOn fst))
    . map prependGroupSpender
    . map mergeByTransaction
    . groupBy (\ a b -> getActor a == getActor b)
    . sortOn getActor
    . (>>= (\ (i, e) -> taggedGroupBySpender i $ fuse $ decomposeEvent e))
    where
        getActor (_, a, _) = a

firstNettingPhase ::
    OwnershipState -> [(Actor, [(TransactionId, Transfer)])] -> [TransactionId]
    -> (Actor, [(TransactionId, Transfer)])
    -> (Actor, [(TransactionId, Transfer)], [TransactionId])
firstNettingPhase s qs rejected (a, q) =
    let
        (prefix, rejected', _) = foldl f ([], rejected, True) q
    in
        (a, prefix, rejected')
    where
        f (prevPrefix, prevRejected, stillLooking) curr@(id, t) =
            let
                incoming = mconcat $ approvedByOthers a rejected qs
                predicted = getBalance a $
                    s <> incoming <> (mconcat (map snd prevPrefix)) <> t
            in
                if positiveBalance predicted
                    && (not $ elem id prevRejected)
                    && stillLooking
                    then (curr:prevPrefix, prevRejected, stillLooking)
                    else (prevPrefix, id:prevRejected, False)

secondNettingPhase ::
    [(Actor, [(TransactionId, Transfer)], [TransactionId])]
    -> ([Event], [TransactionId], [(Actor, [(TransactionId, Transfer)])])
secondNettingPhase = foldl f ([], [], [])
    where
        f (pApproved, pRejected, pState) (cActor, cApproved, cRejected) =
            let
                approved = pApproved ++ map snd cApproved
                rejected = union pRejected cRejected
                state = (cActor, cApproved):pState
            in
                (approved, rejected, state)

approvedByOthers :: Actor -> [TransactionId]
    -> [(Actor, [(TransactionId, Transfer)])]
    -> [Transfer]
approvedByOthers a rejectedIds =
    map snd
    . filter (\(id, _) -> not $ elem id rejectedIds)
    . concat
    . map snd
    . filter (\(a', _) -> a /= a')

nettingLoop :: OwnershipState -> [Event] -> [Event] -> [TransactionId]
    -> [(Actor, [(TransactionId, Transfer)])]
    -> ([Event], [TransactionId])
nettingLoop s prevApproved approved rejected qs =
    if prevApproved == approved
        then (approved, rejected)
        else
            let qs'' = map (firstNettingPhase s qs rejected) qs
                (approved', rejected', qs') = secondNettingPhase qs''
            in
                nettingLoop s approved approved' rejected' qs

net :: OwnershipState -> [(TransactionId, Event)] -> ([Event], [TransactionId])
net s ts =
    let
        queues = makeNettingQueues ts
        initialApproved = map snd ts
    in nettingLoop s [] initialApproved [] queues



-- Sample values for testing

w :: Resource -> Double
w = M.foldl' (<>) mempty . getResource

e :: Event
e = makeOwnershipState' [
        ("a", [("x", -30)]),
        ("b", [("x", 15), ("y", 7), ("z", 8)]),
        ("c", [("z", -10)]),
        ("d", [("x", 5), ("z", 5)])
    ]

d1 :: ([Transfer], Event)
d1 = decomposeEvent e

t :: Transfer
t = makeOwnershipState' [
        ("a", [("x", -30), ("y", 10)]),
        ("b", [("x", 25), ("y", -2), ("z", 5)]),
        ("c", [("x", 5), ("y", -8), ("z", -5)])
    ]

s :: OwnershipState
s = makeOwnershipState' [
        ("a", [("USD", 40)]),
        ("b", [("Bike", 10)]),
        ("c", [("USD", 10)])
    ]

p1 :: Event
p1 = makeOwnershipState' [
        ("a", [("USD", -50), ("Bike", 2)]),
        ("b", [("USD", 50), ("Bike", -3)]),
        ("c", [("Bike", 1)])
    ]

p2 :: Event
p2 = makeOwnershipState' [
        ("a", [("USD", 10)]),
        ("c", [("USD", -10)])
    ]

p3 :: Event
p3 = makeOwnershipState' [
        ("a", [("Bike", -3)]),
        ("c", [("Bike", 3)])
    ]

ps :: [(TransactionId, Event)]
ps = zip [1, 2, 3] [p1, p2, p3]

n = first mconcat $ net s ps

-- The following are all True

-- Event decomposition
ppt0 = isEvent w e
ppt1 = isEvent w e' && isTransformation w e'
    where e' = snd d1
ppt2 = and $ map (\ x -> isTransfer x && isTwoParty x) $ fst d1
ppt3 = (mconcat $ fst d1) <> snd d1 == e
ppt4 = isZero $ w $ M.foldl' (<>) mempty $ getOwnershipState $ snd d1

-- Transfer decomposition
ppt5 = isTransfer t
ppt6 = and $ map (\x -> isTransfer x && isTwoParty x) $ decomposeTransfer t
ppt7 = (mconcat $ decomposeTransfer t) == t
ppt8 = isZero $ snd $ decomposeEvent t

-- Netting

ppt9 = snd n == [3]
ppt10 = fst n == (p1 <> p2)
ppt11 = allBalancesPositive $ (s <> fst n)

-- Global test

pass = and [
        ppt1,
        ppt2,
        ppt3,
        ppt4,
        ppt5,
        ppt6,
        ppt7,
        ppt8,
        ppt9,
        ppt10,
        ppt11
    ]
