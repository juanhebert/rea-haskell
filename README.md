# rea

An implementation of an algebraic resource accounting framework as described in
my MSc thesis in Computer Science at the University of Copenhagen entitled
"Algebraic Resource Accounting for Transfers and Transformations".
